package ru.tsc.panteleev.tm;

import ru.tsc.panteleev.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        run(args);
    }

    public static void run(String[] args) {
        if (args == null || args.length == 0) return;
        final String command = args[0];
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(command);
                break;
        }
    }

    public static void showAbout() {
        System.out.println("Name: Sergey Panteleev");
        System.out.println("E-mail: spanteleev@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("1.5.0");
    }

    public static void showHelp() {
        System.out.printf("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application version. \n", TerminalConst.VERSION);
        System.out.printf("%s - Show terminal commands. \n", TerminalConst.HELP);
    }

    public static void showError(String command) {
        System.err.printf("Error! This command `%s` not supported... \n", command);
    }

}
